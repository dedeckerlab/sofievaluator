#pragma rtGlobals=3     // Use modern global access method and strict wave access.
#include "LocalizerUtilities"

//define the batch size, start frame and number of frames to analyze
constant batch_size 		= 50
constant start_frame 		= 200
constant analyzed_frames 	= 200

Menu "SOFI evaluation" 
	"evalutate SOFI and plot PSF", doall_fe(0,1)
End


Function doall_fe(V_mask, V_plot, [S_inputFile, S_imageName])
	variable V_mask		//if set to 1, then all virtual pixels calculated from pixels on the same line will be ignored in the PSF calculation, as they contain EMCCD artefacts
	variable V_plot		//make plots if set to 1
	string S_inputFile, S_imageName
	
	if(V_plot==0 && V_mask==0)
		DoAlert 0, "Plotting turned on to allow manual PSF plot curing"
		V_plot = 1
	endif	
	
	Make /O/N=9/T W_ParameterNames = {"DateTimeStamp", "Fraction due to bleaching", "tau (s)", "unaccounted signal (%)", "FWHM (px)", "noise (%)", "useful SOFI signal (%)", "unexpected SOFI signal (%)", "SNR peak"}
	
	//if inputData is not passed as argument, prompt the file to be analyzed	
	if(paramIsDefault(S_inputFile))
		ImageLoad/O/T=tiff/S=0/C=1/Q 					//S_fileName is the filename of the loaded image
															//only one image is loaded, because we are only interested in the filename anyway
		if(V_flag == 0)									//user clicked "Cancel"
			abort "No file selected; aborting."
		endif
		
		S_imageName = S_fileName[0,strlen(S_fileName)-5]
		readCCDimages /Q/O/S=(start_frame) /C=(analyzed_frames) /DEST=M_CCDFrames S_path + S_fileName
		Wave M_CCDFrames
	else
		ImageLoad/O/T=tiff/S=0/C=1/Q	S_inputFile
		S_imageName = S_fileName[0,strlen(S_fileName)-5]
		readCCDimages /Q/O/S=(start_frame) /C=(analyzed_frames) /DEST=$S_imageName S_path + S_fileName
		Wave M_CCDFrames = $S_imageName
	endif
   
   //make output data wave
   string S_outputName = S_imageName + "_output"
   Make/O/D/N=(dimsize(W_parameterNames,0)) $S_outputName    	
	wave W_evaluatorOutput = $S_outputName
   
	//put all the data in a separate data folder
	DFREF saveDF = GetDataFolderDFR()
	string S_DFName = NameOfWave(M_CCDFrames) + "_" + NumericalDateStamp() + NumericalTimeStamp()
	NewDataFolder /O/S $S_DFName
    
	//make a panel to see what is going on
	string/G GS_progress = "\JC Getting Ready"
	DoWindow/K ProgressWindow
	NewPanel /FLT/N=ProgressWindow /W=(736,407,1201,506)
	ModifyPanel cbRGB=(65535,49157,16385)
	TitleBox title0,pos={199.00,27.00},size={39.00,32.00},fSize=24,frame=0
	TitleBox title0,variable=GS_progress,anchor= MC
	SetActiveSubwindow _endfloat_
	
	//start filling the output wave
	string S_time = NumericalTimeStamp()
	string S_date = NumericalDateStamp()

	W_evaluatorOutput[0] = str2num(S_date + S_time)
    
	//Show the results in a table
	if(V_plot)
		Edit/K=1/W=(3,40.25,395.25,320) W_ParameterNames,W_evaluatorOutput
		ModifyTable format(Point)=1,width(W_ParameterNames)=161,width(W_evaluatorOutput)=152, sigDigits(W_evaluatorOutput)=14
	endif
	
	//determine what is the contribution of bleaching
	sprintf GS_progress, "\JC%s\rstep 1 of 3\revaluating the contribution of bleaching", S_imageName
	DoUpdate
	evaluateBleaching(M_CCDFrames,S_imageName, W_evaluatorOutput, V_plot)
    
	//estimate the SNR with different levels of smoothing
	sprintf GS_progress, "\JC%s\rstep 2 of 3\restimating the SNR", S_imageName
	DoUpdate
	evaluateSNR(M_CCDFrames,S_imageName, V_plot)
	
	SVAR GS_4pxName
	wave W_4pxHistogram = $GS_4pxName
	W_evaluatorOutput[8] = determineSNRpeak(W_4pxHistogram)
    
	//evaluate the PSF of the SOFI experiment
	sprintf GS_progress, "\JC%s\rstep 3 of 3\revaluating the PSF", S_imageName
	DoUpdate
	evaluatePSF(0,M_CCDFrames,S_imageName, W_evaluatorOutput, V_mask, V_plot)
	
	
	KillWindow/Z ProgressWindow
	KillWaves /Z $S_DFName, M_CCDFrames
	KillStrings/Z GS_4pxName, GS_progress
	
	SetDataFolder saveDF
End


Function determineSNRpeak(W_input)
	wave W_input

	Duplicate/O W_input, W_input_smth
	Smooth 50, W_input_smth
	Differentiate W_input_smth /D=W_input_smth_DIF
	Duplicate/O W_input_smth_DIF, W_input_smth_DIF_smth
	Smooth 50, W_input_smth_DIF_smth

	FindLevels /EDGE=2 /Q W_input_smth_DIF_smth, 0
	wave W_findLevels
	
	KillWaves/Z W_input_smth, W_input_smth_DIF, W_input_smth_DIF_smth
	
	CureLevels(W_input, W_findLevels)
	
	return W_Findlevels[dimsize(W_FindLevels,0)-1]
End


Function CureLevels(W_input, W_levels)
	wave W_input, W_levels
	
	variable i
	for(i=dimsize(W_levels,0)-1; i>=0; i-=1)
		if(W_input(W_levels[i]) < (mean(W_input)/10))
			Redimension /N=(dimsize(W_levels,0)-1) W_Levels
		else
			return 1
		endif
	endfor
End


Function evaluateBleaching(M_CCDFrames,S_name, W_evaluatorOutput, V_plot)
	wave M_CCDFrames
	string S_name
	wave W_evaluatorOutput
	variable V_plot
	
	//plot the decrease in SOFI signal with increasing lag given 0,0,0,1,0,0 weights; under COMB 1, this is a virtual pixel with a high signal and no artefact: (1,0) = (0,0) & (1,0)
	Make/O/N=10 decay 									//The "real" SOFI signal is almost certainly non-existent at a time lag of 10
	decay[] = avgSOFIsignal_lag(p,M_CCDFrames) 	//plot the decrease in SOFI signal for varying lag values
	Variable temp = decay[0]							//normalize to 1 for lag 0
	decay /= temp

	if(V_plot)
		//Plot the decorrelation curve
		string S_GraphName = "decorrelationGraph_" + S_name

		do	//make sure each graph is named uniquely
			DoWindow/F $S_GraphName
			if (V_Flag == 0)
				break
			else
				S_GraphName += "a"
			endif
		while (1)
	
		Display/W=(407.25,40.25,892.5,313.25)/K=1/N=$S_GraphName decay
		SetAxis left -0.05,*
		Label left "Average SOFI signal";Label bottom "time lag"
		ModifyGraph mode(decay)=3, marker(decay)=8
		ModifyGraph margin(top)=28
		DoUpdate
	endif

	//fit the decorrelation function with an exponential
	CurveFit/N/Q/M=0/W=0 exp, decay/D
	wave W_coef,W_sigma
	Variable V_bleachFraction = W_coef[0] / (W_coef[0]+W_coef[1])
	Variable V_tau = 1/ W_coef[2]
	
	W_evaluatorOutput[1] = V_bleachFraction
	W_evaluatorOutput[2] = V_tau
	
	if(V_plot)
		ModifyGraph rgb(fit_decay)=(0,12800,52224)
		DoUpdate
	
		string S_decorrelationResults
		sprintf S_decorrelationResults, "\\JRFraction of SOFI signal due to bleaching: %.*g\rTau: %.*g\r", 3, V_bleachFraction, 3, V_tau
		TextBox/C/N=Summary S_decorrelationResults
	endif
	
	KillWaves W_coef, W_sigma
End


Function evaluatePSF(lag, M_CCDFrames, S_name, W_evaluatorOutput, V_mask, V_plot)
	Variable lag
	wave M_CCDFrames
	string S_name
	wave W_evaluatorOutput
	variable V_mask, V_plot
	
	//consists of three parts
	//1. calculate the avg sofi intensity as a function of distance of seed pixel in combination
	SOFIsignalvsDistance(lag, M_CCDFrames, S_name)
	WAVE psf_x, psf_y 
	
	//2. plot and, remove abberant data points
		//if V_mask = 0, allow this to be done manually
		//if V_mask = 1, just remove the virtual pixel contributions that are calculated from pixels on a horizontal line (EMCCD artefacts)
	PlotAndRemoveDataPoints(psf_x, psf_y, S_name, lag, V_mask, V_plot)
	
	//3. calculate the PSF based on this
	PSFcalculation(lag, M_CCDFrames, S_name, psf_x, psf_y, W_evaluatorOutput, V_plot)
End


Function SOFIsignalvsDistance(lag, M_CCDFrames, S_name)
	Variable lag
	wave M_CCDFrames
	string S_name
	
	string S_CCDFramesFolder = GetWavesDataFolder(M_CCDFrames,2)

	//generate pixel combinations which are maximally distal in the 5x5 grid; there are 40 of these under /COMB = 100
	SOFIPixelCombinations /COMB=100 2
	WAVE M_SOFIPixelCombinations

	Make/O/N=(dimsize(M_SOFIPixelCombinations,0)) psf_x = 0, psf_y = 0

	Make/O/N=(1,dimsize(M_SOFIPixelCombinations,1)) W_current_CWAV

	Variable i
	for(i=0;i<40;i+=1)
		W_current_CWAV[0][] = M_SOFIPixelCombinations[i][q]
		GenerateSOFIoneVirtualPixel(S_CCDFramesFolder, W_current_CWAV)
		wave M_SOFIoneVpx
		psf_y[i] = mean(M_SOFIoneVpx)
		psf_x[i] = sqrt((M_SOFIPixelCombinations[i][2]-M_SOFIPixelCombinations[i][4])^2 + (M_SOFIPixelCombinations[i][3]-M_SOFIPixelCombinations[i][5])^2) / sqrt(2)
	EndFor
	
	KillWaves /Z W_current_CWAV, M_SOFIPixelCombinations, M_SOFIoneVpx
End


Function GenerateSOFIoneVirtualPixel(S_input, W_CWAV)
	string S_input
	wave W_CWAV
	
	NewSOFI /BAT=(batch_size) /Q /CWAV=W_CWAV /LAG={0} /PXCR=0 S_input
	wave M_SOFI
	SetScale/P x 0,1,"", M_SOFI
	SetScale/P y 0,1,"", M_SOFI
	
	if(W_CWAV[0][0]==1)
		Make/O /N=(dimsize(M_SOFI,0)/2,dimsize(M_SOFI,1)) temp
		temp = M_SOFI[2*p+1][q]
		Duplicate/O temp, M_SOFI
	endif
	
	if(W_CWAV[0][1]==1)
		Make/O /N=(dimsize(M_SOFI,0),dimsize(M_SOFI,1)/2) temp
		temp = M_SOFI[p][2*q+1]
		Duplicate/O temp, M_SOFI
	endif
	
	Duplicate/O M_SOFI, M_SOFIoneVpx
	
	Killwaves M_SOFI, temp
End


Function PlotAndRemoveDataPoints(psf_x, psf_y, S_name, lag, V_mask, V_plot)
	wave psf_x, psf_y
	string S_name
	variable lag, V_mask, V_plot

	if(V_plot)
		string S_GraphName = "PSFgraph_" + S_name
	
		do	//make sure each graph is named uniquely
			DoWindow/F $S_GraphName
			if (V_Flag == 0)
				break
			else
				S_GraphName += "a"
			endif
		while (1)
	
		Display/W=(406.5,340.25,891.75,612.5)/K=1 /N=$S_GraphName psf_y vs psf_x
		ModifyGraph margin(top)=28
		Label left "SOFI signal";Label bottom "seed pixel distance"
	
		ShowInfo/CP={0, 1}
		ModifyGraph mode=3
		DoUpdate
	endif
	
	if(V_mask == 0) //this implies that V_plot = 1, because V_mask == 0 && V_plot == 0 is not allowed
		DoWindow/F RemovePointPanel
		if (V_Flag != 0)				//abort function execution if there is still a RemovePointPanel open
			return 0
		endif
		DoWindow/F $S_GraphName	//bring correct graph to front
		NewPanel /N=RemovePointPanel /K=1 /W=(1206,454,1672,565) as "Remove data points from plot"
		DrawText 158,36,"Click to remove data point(s)"
		Button RemoveButton,pos={30.00,19.00},size={113.00,20.00},proc=RemoveButtonProc,title="Remove"
		DrawText 160,79,"Click when done"
		Button DoneButton,pos={30.00,62.00},size={113.00,20.00},proc=DoneButtonProc,title="Done"
	
		PauseForUser RemovePointPanel, $S_GraphName
		HideInfo /W=$S_GraphName
	else
		Make /N=40 MaskWave = 1
		MaskWave[0] = NaN; MaskWave[4] = NaN; MaskWave[12] = NaN; MaskWave[15] = NaN
		psf_x *= MaskWave
		psf_y *= MaskWave
		KillWaves/Z MaskWave
	endif
End


Function RemoveButtonProc(ba) : ButtonControl
	STRUCT WMButtonAction &ba

	switch(ba.eventCode)
		case 2:
			wave psf_x, psf_y
			if(strlen(csrInfo(A)) == 0)
				DoAlert 0, "Cursor A is not in the current graph."
				break
			else
				psf_x[pcsr(A)] = NaN
				psf_y[pcsr(A)] = NaN
				Cursor /K A
			endif
			
			if(strlen(csrInfo(B)) == 0)
				break
			else
				psf_x[pcsr(B)] = NaN
				psf_y[pcsr(B)] = NaN
				Cursor /K B
			endif
			
			if(strlen(csrInfo(C)) == 0)
				break
			else
				psf_x[pcsr(C)] = NaN
				psf_y[pcsr(C)] = NaN
				Cursor /K C
			endif
			
			if(strlen(csrInfo(D)) == 0)
				break
			else
				psf_x[pcsr(D)] = NaN
				psf_y[pcsr(D)] = NaN
				Cursor /K D
			endif
			
	endswitch

	return 0
End


Function DoneButtonProc(ba) : ButtonControl
	STRUCT WMButtonAction &ba

	switch(ba.eventCode)
		case 2:
			KillWindow RemovePointPanel
			variable V_flag = 1
	endswitch

	return 0
End


Function PSFcalculation(lag, M_CCDFrames, S_name, psf_x, psf_y, W_evaluatorOutput, V_plot)
	variable lag
	wave M_CCDFrames, psf_x, psf_y
	string S_name
	wave W_evaluatorOutput
	variable V_plot
	
	string S_CCDFramesFolder = GetWavesDataFolder(M_CCDFrames,2)

	//calculate the total signal that will be plotted later
	Make/O/N=9 wts = 0; wts[0] = 1
	NewSOFI /SAME /BAT=(batch_size) /Q /WGHT=wts /LAG={lag} /PXCR=0 /COMB=1 S_CCDFramesFolder
	Wave M_SOFI
	
	Make/O/N=((Dimsize(M_SOFI,0)/2),(Dimsize(M_SOFI,1)/2)) sublim
	sublim = M_SOFI[2*p][2*q]
	Variable V_totalSignal = mean(sublim)
	
	//fit the curve with a gaussian, helped by seeding a few parameters
	variable V_FitError = 0
	
	K0 = wavemin(psf_y)							//x=inf => y = y0 = K0
	K1 = wavemax(psf_y) - wavemin(psf_y)	//x=0 => y = y0 + A = K0 + K1
	K2 = 0											//x0 = K3 (no offset in x direction)
	K3 = 2
	CurveFit/Q/N/G/H="0010"/NTHR=0/M=0 gauss psf_y /X=psf_x /D
	wave W_sigma, W_coef
	DoUpdate
	
	string S_FitError
	sprintf S_FitError, "V_fiterror is %g at %s", V_fitError, nameofwave(M_CCDframes)
	print S_FitError
	
	//calculate and display two important parameters on the graph
	Variable quality = W_coef[0] / (W_coef[0]+W_coef[1]) //fraction of unacounted signal (< fraction bleaching)
	Variable FWHM = W_coef[3] * 2*sqrt(ln(2))
	
	W_evaluatorOutput[3] = quality
	W_evaluatorOutput[4] = FWHM
	
	if(V_plot)
		string S_psfResults
		sprintf S_psfResults, "\\JRFraction unaccounted signal = %.*g %%\r Estimated FWHM (pixels) = %.*g\r", 2, quality*100, 3, FWHM
		TextBox/C/N=Summary S_psfResults
	endif
	
	//make a barchart that displays some extra "free" quality measures
	Make/O/N=3 barchart
	
	Variable V_noise      = V_totalSignal - W_coef[0] - W_coef[1]
	Variable V_unexpected = W_coef[0]
	Variable V_useful     = W_coef[1]
	
	Variable V_sumPCT = (V_noise + V_unexpected + V_useful) / 100
	
	W_evaluatorOutput[5] = V_noise / V_sumPCT
	W_evaluatorOutput[6] = V_useful / V_sumPCT
	W_evaluatorOutput[7] = V_unexpected / V_sumPCT
	
	if(V_plot)
		DisplayBarchart(V_noise, V_useful, V_unexpected, S_name)
	endif
	
	KillWaves wts, M_SOFI, sublim, W_sigma, W_coef, barchart
End


Function avgSOFIsignal_lag(V_timelag,M_CCDFrames) //calculate average SOFI pixel value accross whole image for time lag x with WGHT {0,0,0,1,0,0}
	Variable V_timelag
	wave M_CCDFrames

	string S_CCDFramesFolder = GetWavesDataFolder(M_CCDFrames,2)
	
	Make/O/N=6 wts=0; wts[3] = 1
	
	NewSOFI /BAT=(batch_size)/Q/LAG={V_timelag} /WGHT=wts /PXCR=0 /COMB=1 S_CCDFramesFolder
	WAVE M_SOFI
	
	variable temp = mean(M_SOFI)
	
	killwaves M_SOFI, wts
	return temp
End


Function evaluateSNR(M_CCDFrames, S_name, V_plot)
	wave M_CCDFrames
	string S_name
	variable V_plot
	
	string S_CCDFramesFolder = GetWavesDataFolder(M_CCDFrames,2)
	
	Make/N=6 wts = 0; wts[0,1] = 1

	NewSOFI /BAT=(batch_size)/Q /WGHT=wts /JACK /PXCR=0 /COMB=1 S_CCDFramesFolder
	WAVE M_SOFI,M_SOFI_jack
	
	ImageTransform AverageImage M_SOFI_jack
	WAVE M_StdvImage,M_AveImage
	M_SOFI /= M_StdvImage * sqrt(analyzed_frames)	//M_SOFI is the signal, the noise is the SD of the 200 images coming from the Jackknife, but with a correction factor of sqrt(n) as per Jackknife theory
	
	Make/N=((Dimsize(M_SOFI,0)/2),(Dimsize(M_SOFI,1)/2)) sublim
	sublim = M_SOFI[2*p][2*q]
	
	Make/O /N=0 W_filters
	W_filters = {0, 1, 2, 4}							//W_filters is in units of real pixels
	
	variable i
	
	if(V_plot)
		string S_filteredHistogramName
		string S_SNRname = "SNRgraph_" + S_name

		do	//make sure each graph is named uniquely
			DoWindow/F $S_SNRName
			if (V_Flag == 0)
				break
			else
				S_SNRName += "a"
			endif
		while (1)
	
		Display/W=(904.5,40.25,1389.75,313.25)/K=1/N=$S_SNRname
		ModifyGraph margin(top)=28
	endif
	
	for(i=0; i<dimsize(W_filters,0); i+=1)
		S_filteredHistogramName = "W_SNRhist_" + num2str(W_filters(i)) + "px"
		Make /O/N=500 W_currentHistogram
		
		FFT /DEST=aFT sublim													//a gaussian convolution is same as the Fourier transform of the products of the Fourier transforms
		aFT *= exp(-(x^2+y^2) * W_filters[i]^2 * pi^2 /4 /ln(2))		//the exponential function is the Fourier transform of a gauss function with FWHM of W_filters[i]
		IFFT /DEST=filtered aFT
		
		Histogram/B=1 filtered, W_currentHistogram
		Duplicate/O W_currentHistogram, $S_filteredHistogramName
		if(V_plot)
			AppendToGraph $S_filteredHistogramName
			DoUpdate
		endif
	endfor
	String/G GS_4pxName = S_filteredHistogramName
	
	if(V_plot)
		Label left "frequency";Label bottom "SNR"
	
		ModifyGraph rgb(W_SNRhist_1px)=(65535,43690,0),rgb(W_SNRhist_2px)=(1,65535,33232),rgb(W_SNRhist_4px)=(0,0,65535)
		Legend/C/N=text0/F=0/H={0,2,10}/A=MC/X=30.59/Y=35.07
		DoUpdate
	endif
	
	KillWaves/Z wts, M_SOFI, M_SOFI_jack, M_StdvImage, M_AveImage, sublim, aFT, filtered, W_currentHistogram
End


Function DisplayBarchart(V_noise, V_useful, V_unexpected, S_name)
	variable V_noise, V_useful, V_unexpected
	string S_name

	Make/O/N=3 barchart = {V_noise, V_useful, V_unexpected}
	//normalize the variable to sum up to 100%
	Variable temp = (V_noise + V_useful + V_unexpected) / 100
	V_noise /= temp
	V_useful /= temp
	V_unexpected /= temp

	Make/O/N=2 barchartX 
	barchartX[0] = 0
	barchartX[1] = 1
	
	Make/O/N=(2,3) barchartY
	barcharty[][0] = 100
	barcharty[][1] = 100 - V_noise
	barcharty[][2] = 100 - V_noise - V_useful

	string S_barchartName = "contributions_" + S_name
	
	do	//make sure each graph is named uniquely
		DoWindow/F $S_barchartName
		if (V_Flag == 0)
			break
		else
			S_barchartName += "a"
		endif
	while (1)

	Display /W=(904.5,339.5,1188,611)/K=1/N=$S_barchartName barchartY[*][0], barcharty[*][1], barcharty[*][2] vs barchartX
	
	ModifyGraph margin(left)=35,margin(bottom)=15,margin(top)=28,margin(right)=170
	ModifyGraph mode=7
	ModifyGraph rgb(barcharty#1)=(0,0,52224),rgb(barcharty#2)=(0,39168,0)
	ModifyGraph hbFill=2
	ModifyGraph tick(bottom)=3, nticks(bottom)=0, noLabel(bottom)=2, standoff(bottom)=0
	Label left "% of signal\r"
	SetAxis left 0,100
	
	Legend/C/N=text0/J/F=0/M/H={0,3,10}/A=MC/X=162.30/Y=31.92 "\\s(barcharty) noise\r\\s(barcharty#1) useful SOFI signal\r\\s(barcharty#2) unexpected SOFI signal"
	TextBox/C/N=text1/F=0/M/H={0,3,10}/A=MC/X=63.93/Y=60.38 "\\Z11 contributions to apparent blinking"
End


Function/S NumericalDateStamp()
	string dstr
	
	Variable datestamp = DateTime
	
	dstr = Secs2Date(datestamp, -2)	//returns date as YYYY-MM-DD
	//remove separator character
	dstr = dstr[0,3] + dstr[5,6] + dstr[8,9]
	
	return dstr
End


Function/S NumericalTimeStamp()
	string tstr
	
	Variable timestamp = DateTime
	
	tstr = Secs2Time(timestamp, 3)	//returns time as hh:mm:ss
	//remove separator character
	tstr = tstr[0,1] + tstr[3,4] + tstr[6,7]
	
	return tstr
End